from django.shortcuts import render, get_object_or_404
from .models import Book
from .forms import BookForm, SearchForm, AddBookForm, SearchBookByDataForm
import requests
from json.decoder import JSONDecodeError


def main_page(request):
    return render(request,
                  'book/mainPage.html')


def book_search(request):
    return render(request,
                  'book/book_search.html')


def book_list(request):
    object_list = Book.object.all()
    return render(request,
                  'book/booklist.html',
                  {'books': object_list})


def add_new_book(request):
    value = False
    if request.method == 'POST':
        book_form = BookForm(data=request.POST)
        if book_form.is_valid():
            new_book = book_form.save(commit=False)
            new_book.save()
            value = True
    else:
        book_form = BookForm()

    return render(request,
                  'book/newBook.html',
                  {'book_form': book_form,
                   'value': value})


def edit_book(request, title, author, isbn_number, page_count):
    value = False
    book = get_object_or_404(Book,
                             title=title,
                             author=author,
                             isbnNumber=isbn_number,
                             pageCount=page_count)
    edit_form = BookForm(instance=book)
    if request.method == 'POST':
        book_form = BookForm(data=request.POST)
        if book_form.is_valid():
            book = book_form.save(commit=False)
            book.save()
            book = get_object_or_404(Book,
                                     title=title,
                                     author=author,
                                     isbnNumber=isbn_number,
                                     pageCount=page_count)
            book.delete()
            value = True
    return render(request,
                  'book/edit_book.html',
                  {'book': book,
                   'edit_form': edit_form,
                   'value': value})


def book_search_by_title(request):
    form = SearchForm()
    query = None
    results_by_title = []
    results_by_author = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            results_by_title = Book.object.filter(title__contains=query)
            results_by_author = Book.object.filter(author__contains=query)
    return render(request,
                  'book/book_search_by_title.html',
                  {'form': form,
                   'query': query,
                   'results_by_title': results_by_title,
                   'results_by_author': results_by_author, })


def book_search_by_date(request):
    form = SearchBookByDataForm
    results_by_date = []
    if 'od' and 'do' in request.GET:
        form = SearchBookByDataForm(request.GET)
        if form.is_valid():
            od = form.cleaned_data['od']
            do = form.cleaned_data['do']
            results_by_date = Book.object.filter(published__range=[od, do])
    return render(request,
                  'book/book_search_by_date.html',
                  {'form': form,
                   'results_by_date': results_by_date})


def test_json(request):
    form = AddBookForm()
    value = False
    if 'add' in request.GET:
        form = AddBookForm(request.GET)
        if form.is_valid():
            try:
                add = form.cleaned_data['add']
                response = requests.get(add)
                data = response.json()
                title = data['items'][0]['volumeInfo']['title']
                author = data['items'][0]['volumeInfo']['authors'][0]
                isbn = data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
                isbn = isbn[:8]
                page_count = data['items'][0]['volumeInfo']['pageCount']
                language = data['items'][0]['volumeInfo']['language']
                publish_date = data['items'][0]['volumeInfo']['publishedDate']
                if len(publish_date) == 7:
                    publish_date = publish_date + '-01'
                if len(publish_date) == 4:
                    publish_date = publish_date + '-01-01'
                image_links = data['items'][0]['volumeInfo']['canonicalVolumeLink']
                new_book = Book(title=title, author=author, isbnNumber=isbn, pageCount=page_count,
                                imageLinks=image_links, language=language, published=publish_date)
                new_book.save()
                value = True
                return render(request, 'book/json.html', {
                    'form': form,
                    'value': value,
                })
            except KeyError:
                return render(request, 'book/json_error.html', {
                })
            except JSONDecodeError:
                return render(request, 'book/json_error.html', {
                })
    return render(request, 'book/json.html', {
        'form': form,
        'value': value
    })


def delete_book(request, author, isbn_number, page_count):
    book = get_object_or_404(Book,
                             author=author,
                             isbnNumber=isbn_number,
                             pageCount=page_count)
    value = False
    if request.method == "POST":
        book.delete()
        value = True
        return render(request,
                      'book/book_delete.html',
                      {'value': value})
    return render(request,
                  'book/book_delete.html',
                  {'book': book,
                   'value': value})


from django.urls import path
from . import views

app_name = 'bookapp'

urlpatterns = [
    path('book_list',
         views.book_list,
         name='book_list'),
    path('newBook',
         views.add_new_book,
         name='add_new_book'),
    path('',
         views.main_page,
         name='mainPage'),
    path('<title>/<author>/<isbn_number>/<int:page_count>/',
         views.edit_book,
         name='edit_book'),
    path('book_search',
         views.book_search,
         name='book_search'),
    path('book_search_by_title',
         views.book_search_by_title,
         name='book_search_by_title'),
    path('test_json',
         views.test_json,
         name='test_json'),
    path('json_error',
         views.test_json,
         name='json_error'),
    path('<author>/<isbn_number>/<int:page_count>/',
         views.delete_book,
         name='delete_book'),
    path('book_search_by_date',
         views.book_search_by_date,
         name='book_search_by_date')
]

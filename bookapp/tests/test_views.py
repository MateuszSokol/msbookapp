from django.urls import reverse
from django.test import RequestFactory
from mysite.bookapp.views import main_page, book_search


class TestViews:

    def test_main_page(self):
        path = reverse('bookapp:mainPage')
        request = RequestFactory().get(path)
        response = main_page(request)

        assert response.status_code == 200

    def test_book_search(self):
        path = reverse('bookapp:book_search')
        request = RequestFactory().get(path)
        response = book_search(request)

        assert response.status_code == 200


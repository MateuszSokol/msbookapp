from django.db import models
from django.urls import reverse


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    isbnNumber = models.CharField(max_length=100)
    pageCount = models.IntegerField()
    imageLinks = models.TextField()
    language = models.TextField()
    published = models.DateField(auto_now=False)
    object = models.Manager()

    def get_absolute_url(self):
        return reverse('bookapp:edit_book',
                       args=[self.title,
                             self.author,
                             self.isbnNumber,
                             self.pageCount])

    def delete_book(self):
        return reverse('bookapp:delete_book',
                       args=[self.author,
                             self.isbnNumber,
                             self.pageCount])

    def __str__(self):
        return self.title



from django import forms
from .models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ('title', 'author', 'isbnNumber', 'pageCount', 'imageLinks', 'language', 'published')


class SearchForm(forms.Form):
    query = forms.CharField()


class SearchBookByDataForm(forms.Form):
    od = forms.DateField()
    do = forms.DateField()


class AddBookForm(forms.Form):
    add = forms.URLField()

from django.urls import path
from . import views

app_name = 'bookapp'

urlpatterns = [
    path('bookapi/',
         views.BookListView.as_view(),
         name='bookapi'),
]

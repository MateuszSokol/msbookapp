from rest_framework import generics
from ..models import Book
from .serializers import BookSerializer


class BookListView(generics.ListAPIView):
    serializer_class = BookSerializer

    def get_queryset(self):
        qs = Book.object.all()
        title = self.request.GET.get("title")
        if title is not None:
            qs = qs.filter(title__contains=title)
        author = self.request.GET.get('author')
        if author is not None:
            qs = qs.filter(author__contains=author)
        return qs


class BookDetailView(generics.RetrieveAPIView):
    queryset = Book.object.all()
    serializer_class = BookSerializer

